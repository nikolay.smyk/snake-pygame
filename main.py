import pygame
from conf import SIDE, FPS, DELAY, BLACK, RED
from heroes import Snake, Snack


pygame.init()
timer = pygame.time.Clock()
screen = pygame.display.set_mode((SIDE, SIDE))
snake = Snake()
snack = Snack(snake)
font = pygame.font.SysFont(pygame.font.get_default_font(), 50)
img = font.render('GAME OVER', True, RED)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        # Controls
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                snake.turn_left()
            if event.key == pygame.K_RIGHT:
                snake.turn_right()
            if event.key == pygame.K_DOWN:
                snake.turn_down()
            if event.key == pygame.K_UP:
                snake.turn_up()

    timer.tick(FPS)                     # Tick next frame
    pygame.time.delay(DELAY)            # Do some FPS magic
    screen.fill(BLACK)

    snake.move()
    snake.eat_snack(snack)
    if snake.has_collided():
        screen.blit(img, (80, 80))
        running = False
    snake.draw(screen)
    snack.draw(screen)

    pygame.display.flip()               # Draw all content on screen

pygame.time.wait(8000)
pygame.quit()
