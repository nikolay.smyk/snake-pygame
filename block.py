import pygame
from conf import SIDE, ROWS, WHITE


class Block:
    """Base of every in-game object"""
    face = SIDE // ROWS
    dir_left = dir_up = -1
    dir_right = dir_down = 1
    dir_static = 0
    directions = [dir_left, dir_right, dir_up, dir_down, dir_static]

    def __init__(self, pos_x: int, pos_y: int, color: tuple[int, int, int] = WHITE):
        self._pos_x = pos_x
        self._pos_y = pos_y
        self._prev_pos_x = pos_x
        self._prev_pos_y = pos_y
        self._dir_x = self.dir_static
        self._dir_y = self.dir_static
        self._color = color

    def __repr__(self):
        return f'{self.__class__.__name__}(pos_x={self._pos_x}, posy={self._pos_y}, ' \
               f'prev_x={self._prev_pos_x}, prev_y={self._prev_pos_y})'

    @property
    def pos_x(self):
        return self._pos_x

    @property
    def pos_y(self):
        return self._pos_y

    @property
    def pos(self):
        return self._pos_x, self._pos_y

    @property
    def prev_pos_x(self):
        return self._prev_pos_x

    @property
    def prev_pos_y(self):
        return self._prev_pos_y

    @property
    def dir_x(self):
        return self._dir_x

    @property
    def dir_y(self):
        return self._dir_y

    @staticmethod
    def is_valid_pos(val: int) -> bool:
        """Make sure block doesn't get positioned outside of gamefield grid"""
        if not 0 <= val < ROWS:
            raise ValueError(f'This position is out of game field: {val}')
        return True

    @pos_x.setter
    def pos_x(self, val: int):
        self.is_valid_pos(val)
        self._pos_x = val

    @pos_y.setter
    def pos_y(self, val: int):
        self.is_valid_pos(val)
        self._pos_y = val

    def save_prev_pos(self):
        """Saving current position as previous one"""
        self._prev_pos_x = self._pos_x
        self._prev_pos_y = self._pos_y

    def draw(self, surface: pygame.display):
        """
        Drawing a rectangle on imaginary grid using x, y position values
        to find coordinates on screen
        """
        x = self._pos_x * self.face
        y = self._pos_y * self.face
        pygame.draw.rect(surface, self._color, pygame.Rect(x, y, self.face, self.face))

    def point_up(self) -> None:
        """Set block to face up"""
        self._dir_y = self.dir_up
        self._dir_x = self.dir_static

    def point_down(self) -> None:
        """Set block to face down"""
        self._dir_y = self.dir_down
        self._dir_x = self.dir_static

    def point_left(self) -> None:
        """Set block to face left"""
        self._dir_x = self.dir_left
        self._dir_y = self.dir_static

    def point_right(self) -> None:
        """Set block to face right"""
        self._dir_x = self.dir_right
        self._dir_y = self.dir_static

    def is_faced_up(self) -> bool:
        return self._dir_y == self.dir_up

    def is_faced_down(self) -> bool:
        return self._dir_y == self.dir_down

    def is_faced_left(self) -> bool:
        return self._dir_x == self.dir_left

    def is_faced_right(self) -> bool:
        return self._dir_x == self.dir_right

    def is_static(self) -> bool:
        """Check if block has no direction set"""
        return self._dir_x == self.dir_static and self._dir_y == self.dir_static
