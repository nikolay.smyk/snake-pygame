"""Configuration and basic constants for snake game"""

# Windows draw settings
SIDE = 400
ROWS = 20
FPS = 10
DELAY = 0

# Color codes
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
BRICK = (204, 205, 0)
RED = (204, 0, 0)
