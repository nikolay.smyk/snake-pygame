"""Contains all interactive objects for snake game"""
from random import choice
from pygame import display
from conf import ROWS, BRICK, GREEN
from block import Block


class Snake:
    """Hssss! I'm sssssnake!!!"""
    color = BRICK

    def __init__(self):
        center_block_pos = ROWS // 2
        self._body = [Block(center_block_pos, center_block_pos, self.color)]

    @property
    def lng(self) -> int:
        """Count current length of snake"""
        return len(self._body)

    @property
    def head(self) -> Block:
        """Snake's head is the first block in body"""
        return self._body[0]

    @property
    def body(self) -> list:
        """Return snake's body"""
        return self._body

    @property
    def body_posns(self) -> list[tuple[int, int]]:
        """Return list of (x, y) coordinates of snake's body parts"""
        return [body_part.pos for body_part in self._body[1::]]

    def __str__(self):
        return f'{self.head}, lng={len(self._body)}'

    def draw(self, surface: display) -> None:
        """Draw each of snake's body parts"""
        for body_block in self._body:
            body_block.draw(surface)

    def move(self) -> None:
        """
        Move head according to direction the block is facing,
        for the rest of the blocks - move to the previous position of block ahead of it
        """
        def calc_pos(pos_val: int) -> int:
            """
            Handle edge screen teleport: if new position of block is outside of grid
            shift it to the edge opposite side
            """
            if pos_val < 0:
                return ROWS - 1
            if pos_val >= ROWS:
                return 0
            return pos_val

        for i, body_block in enumerate(self._body):
            body_block.save_prev_pos()      # save previous position before moving
            if i == 0:
                # snake's head moves in set direction
                if body_block.is_static():
                    continue
                if body_block.is_faced_left():
                    body_block.pos_x = calc_pos(body_block.pos_x - 1)
                if body_block.is_faced_right():
                    body_block.pos_x = calc_pos(body_block.pos_x + 1)
                if body_block.is_faced_down():
                    body_block.pos_y = calc_pos(body_block.pos_y + 1)
                if body_block.is_faced_up():
                    body_block.pos_y = calc_pos(body_block.pos_y - 1)
            else:
                # all blocks after head move to prev position of block ahead
                block_ahead = self._body[i-1]
                body_block.pos_x = block_ahead.prev_pos_x
                body_block.pos_y = block_ahead.prev_pos_y

    def __add_block(self):
        """Add block to the tail"""
        tail = self._body[-1]
        self._body.append(Block(tail.prev_pos_x, tail.prev_pos_y, self.color))

    def eat_snack(self, snack: 'Snack'):
        """If snake's head meets snack - add new block to snake"""
        if snack.is_eaten():
            self.__add_block()

    def turn_left(self):
        """Point head left if it's not facing right"""
        if not self.head.is_faced_right():
            self.head.point_left()

    def turn_right(self):
        """Point head right if it's not facing left"""
        if not self.head.is_faced_left():
            self.head.point_right()

    def turn_down(self):
        """Point head down if it's not facing up"""
        if not self.head.is_faced_up():
            self.head.point_down()

    def turn_up(self):
        """Point head up if it's not facing down"""
        if not self.head.is_faced_down():
            self.head.point_up()

    def has_collided(self) -> bool:
        """Checks if snake's head bumps into own body"""
        if self.lng > 1:
            return False
        return self.head.pos in self.body_posns[1::]     # Excluding head from search


class Snack:
    """A snack that snake should eat to grow"""
    color = GREEN
    _pos_x: int
    _pos_y: int

    def __init__(self, snake: Snake):
        self._snake = snake
        self.change_pos()

    def __str__(self):
        return f'Snack(x={self._pos_x}, y={self._pos_y}, is_eaten:{self.is_eaten()})'

    @property
    def pos(self) -> tuple[int, int]:
        """Returns (x, y) positions of snack"""
        return self._pos_x, self._pos_y

    def change_pos(self) -> None:
        """Set snack on new random position on field that's not covered by snake"""
        available_grid = [(x, y) for y in range(ROWS) for x in range(ROWS)
                          if (x, y) not in self._snake.body_posns]
        self._pos_x, self._pos_y = choice(available_grid)

    def is_eaten(self) -> bool:
        """Snack is considered eaten if its' coordinates match coordinates of snake's head"""
        return self.pos == self._snake.head.pos

    def draw(self, surface: display):
        """Re-position snack if it got eaten and draw it"""
        if self.is_eaten():
            self.change_pos()
        Block(self._pos_x, self._pos_y, self.color).draw(surface)
